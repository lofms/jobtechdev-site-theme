
// container with active filters
var activeFilter = [];
var forceFilter = [];

function passesFilter(tags, filter) {
	for(var j=0; j<filter.length; ++j) {
		if(tags.indexOf(filter[j]) == -1) {
			return false;
		}
	}
	return true;
}

function updateFilteredItems() {
	var root = document.getElementById('component_list');
	for(var i=0; i<root.children.length; ++i) {
		var child = root.children[i];
		var visible = passesFilter(child.id, activeFilter) && passesFilter(child.id, forceFilter);
		child.style.display = visible ? "flex" : "none";
	}
}

function setFieldFilter(selectedIndex, tags) {
	var values = [];
	for(var i in tags) {
		values.push(tags[i]);
	}
	var index = forceFilter.indexOf(values[selectedIndex]);
	if(index == -1) {
		// remove fields
		for(var i=0; i<values.length; ++i) {
			index = forceFilter.indexOf(values[i]);
			if(index != -1) {
				forceFilter.splice(index, 1);
			}
		}
		// add field
		forceFilter.push(values[selectedIndex]);
		// update view
		updateFilteredItems();
	}
}

function toggleFilter(caller, tag) {
	var index = activeFilter.indexOf(tag);
	if(index != -1) {
		// deselect tag
		activeFilter.splice(index, 1);
		caller.style.background = "unset";
		caller.style.color = "unset";
	} else {
		// select tag
		activeFilter.push(tag);	
		caller.style.background = "#000";
		caller.style.color = "#ddd";
	}
	updateFilteredItems();
}

function setLanguage(lang) {
	if(window.location.pathname.length >= 3) {
		var path = window.location.pathname.substring(4).split('/');
		var root = path[0];
		if(lang == "en") {
			if(root == "om-jobtech-development") {
				root = "about-jobtech-development";
			} else if(root == "produkter") {
				root = "products";
			} else if(root == "nyheter") {
				root = "news";
			} else if(root == "jobba-hos-oss") {
				root = "work-with-us";
			} else if(root == "om-webbplatsen") {
				root = "about-the-website";
			} else if(root == "kontakt") {
				root = "contact-us";
			} else if(root == "kalender") {
				root = "calendar";
			}
		} else {
			if(root == "about-jobtech-development") {
				root = "om-jobtech-development";
			} else if(root == "products") {
				root = "produkter";
			} else if(root == "news") {
				root = "nyheter";
			} else if(root == "work-with-us") {
				root = "jobba-hos-oss";
			} else if(root == "about-the-website") {
				root = "om-webbplatsen";
			} else if(root == "contact-us") {
				root = "kontakt";
			} else if(root == "calendar") {
				root = "kalender";
			}
		}
		path[0] = root;
		// rebuild uri
		var target = "/" + lang;
		for(var i=0; i<path.length; ++i) {
			target = target + "/" + path[i];
		}
		window.location.pathname = target;
	} else {
		window.location.pathname = "/" + lang;
	}
}

function removeElement(e) {
	if(e && e.parentNode) {
		e.parentNode.removeChild(e);
	}
}

function toggleStyle(element, style) {
	if(element.classList) {
		element.classList.toggle(style);
	} else {
		// IE9
		var classes = element.className.split(" ");
		var i = classes.indexOf(style);
		if (i >= 0) {
			classes.splice(i, 1);
		} else {
			classes.push(style);
		}
		element.className = classes.join(" ");
	}
}

function mobileMenuClose() {
	var menu = document.getElementById("main_menu");
	var menuButton = document.getElementById("main_menu_button");
	if(menu.classList) {
		menu.classList.add("hidden_menu");
		menuButton.classList.remove("close");
	} else {
		// IE9
		var classes = menu.className.split(" ");
		var i = classes.indexOf("hidden_menu");
		if (i == -1) {
			classes.push("hidden_menu");
			menu.className = classes.join(" ");
			// change button icon
			menuButton.className = menuButton.className.split(" ")[0];
		}
	}
}

function mobileMenuToggle(element) {
	var menu = document.getElementById("main_menu");
	var menuButton = document.getElementById("main_menu_button");
	toggleStyle(menu, "hidden_menu");
	toggleStyle(menuButton, "close");
}

function mobileTouchHook() {
	document.addEventListener("mousedown", function(e) {
		var menu = document.getElementById("main_menu");
		var menuButton = document.getElementById("main_menu_button");
		if(e.path.indexOf(menu) == -1 && e.path.indexOf(menuButton) == -1) {
			mobileMenuClose();
		}
	});
}

function acceptCookies() {
	localStorage.setItem("jtCookiesAccepted", true);
	removeElement(document.getElementById('cookie_notification'));
}

function statusClicked() {
	sessionStorage.setItem("jtStatusAccepted", Date.now());
	removeElement(document.getElementById('status_notification'));
}

var callback = function() {
	mobileTouchHook();
	// check if we should display cookie notification
	if(localStorage.getItem("jtCookiesAccepted") == null) {
		document.getElementById("cookie_notification").style.display = "flex";
	}
	// check if we should show error message
	if(sessionStorage.getItem("jtStatusAccepted") != null) {
		var elapsed = Date.now() - parseInt(sessionStorage.getItem("jtStatusAccepted"));
		if(elapsed < 60 * 60 * 1000) {
			removeElement(document.getElementById('status_notification'));
		}
	}
	// apply filter 
	var raw = decodeURIComponent(window.location.hash);
	var index = raw.indexOf("f=");
	if(index != -1) {
		raw = raw.substring(index + 2);
		if(raw.indexOf('&') != -1) {
			raw = raw.substring(0, raw.indexOf('&'));
		}
		var element = document.getElementById("tag_" + raw);
		if(element != null) {
			toggleFilter(element, raw);
		}
	}
};

if(document.readyState === "complete" || (document.readyState !== "loading" && !document.documentElement.doScroll)) {
	callback();
} else {
	document.addEventListener("DOMContentLoaded", callback);
}








